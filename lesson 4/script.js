const board = document.querySelector("#board");
const colors = ['purple', 'pink', 'palevioletred', 'plum', 'blueviolet', 'rebeccapurple']
const SQUARES_NUMBER = 500

for (let i = 0; i < SQUARES_NUMBER; i++) {
    const square = document.createElement('div')
    square.classList.add('square')

    square.addEventListener('mouseover', () => {
        setColor(square)
    })
    square.addEventListener('mouseleave', () => {
        removeColor(square)
    })

    board.append(square)
}

function setColor(element) {
    const color = getRandomColor()
    element.style.backgroundColor = color
    element.style.boxShadow = `0 0 2px ${color}, 0 0 10 px ${color}`
}

function removeColor(element) {
    element.style.backgroundColor = 'rgb(53, 53, 53)'
    element.style.boxShadow = `0 0 2px black`
}

function getRandomColor() {
    const index = Math.floor(Math.random() * colors.length)
    return colors[index]
}